<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use App\Services\MercadoPagoService;
use Log;

class HomeController extends Controller
{

    public function getItem(Request $request)
    {
        $items = [
            [
                "id"=>1234,
                "title" => $request->title,
                "quantity" => intval($request->unit),
                "description" => "“​ Dispositivo móvil de Tienda e-commerce​ ”",
                "picture_url"=>"http://cfp.sysfreelance.com.ar/bizpage/img/intro-carousel/5.jpg",
                "currency_id" => "ARS",
                "unit_price" => (double) $request->price
            ],
        ];
        
        $payer = [
            "email" => 'test_user_63274575@testuser.com',
            "surname" => 'Lalo Landa',
            "phone" => [
                'area_code'=>'11',
                'number'=>'22223333'
            ],
            "address" => [
                "zip_code" => "1111",
                "street_name"=> false,
                "street_number"=> "123"
            ]
        ];

        try {
            $mp = resolve(MercadoPagoService::class);
            $r = $request->all();
            $preference = $mp->createPreference($items, $payer);
            return view('detail',compact('r','preference'));
        } catch (Exception $e){
            dd($e->getMessage());
        }
    }

    public function success(Request $request)
    {
        Log::info('preference_id: '.$request->get('preference_id'));
        Log::info('colletion_id: '.$request->get('collection_id'));
        $mp = resolve(MercadoPagoService::class);
        $response = $mp->capturePayment($request->get('collection_id'));
        return view('success', compact('response'));
    }

    public function pending(Request $request)
    {
        $response = $request->all();
        return view('pending', compact('response'));

    }
    public function failure(Request $request)
    {
        // $response = $request->all();
        // return view('failure', compact('response'));
        return redirect()->route('welcome')->with('message-failure','Ups!! =( vuelve a intentarlo!');

    }
    
    public function ipn(Request $request) {
        Log::info('IPN id= '.$request->id);

        if ($request->id) {

            //se obtiene la info del pago usando el id que viene en el ipn
            try{
                $mp = resolve(MercadoPagoService::class);
                $rpta = $mp->capturePayment($request->id);
                Log::info('idMpago = '.$request->id);
            }catch(Exception $e){
                Log::error('no se registro '.$request->id);
            }

            // si el estado es OK
            if ($rpta->status == 200) {

                return \Response::json(['HTTP/1.1 200 OK'], 200);

            }
        }

    }
}
