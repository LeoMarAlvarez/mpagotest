<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Traits\ConsumesExternalServices;

class MercadoPagoService
{
    use ConsumesExternalServices;

    protected $baseUri;

    protected $key;

    protected $secret;

    protected $baseCurrency;

    protected $integratorId;

    public function __construct()
    {
        $this->baseUri = config('services.mercadopago.base_uri');
        $this->key = config('services.mercadopago.key');
        $this->secret = config('services.mercadopago.secret');
        $this->baseCurrency = config('services.mercadopago.base_currency');
        $this->integratorId = config('services.mercadopago.integrator_id');
    }

    public function resolveAuthorization(&$queryParams, &$formParams, &$headers)
    {
        $queryParams['access_token'] = $this->resolveAccessToken();
    }

    public function decodeResponse($response)
    {
        return json_decode($response);
    }

    public function resolveAccessToken()
    {
        return $this->secret;
    }

    public function createPreference($items, $payer)
    {
        return $this->makeRequest(
            'POST',
            '/checkout/preferences',
            [],
            [
                'items'=>$items,
                'payer'=>$payer,
                'payment_methods' => [
                    'excluded_payment_methods' => [
                        ['id'=>'amex'],
                    ],
                    'excluded_payment_types' => [
                        ['id'=>'atm']
                    ],
                    'installments'=>6
                ],
                'back_urls' => [
                    'success' => route('success'),
                    'pending' => route('pending'),
                    'failure' => route('failure')
                ],
                'notification_url' => route('ipn'),
                'auto_return' => 'approved',
                'external_reference' => 'alvarezleom@gmail.com',
                'marketplace' => "Tienda e-commerce"
            ],
            [
                'x-integrator-id'=>$this->integratorId
            ],
            $isJsonRequest = true
        );
    }

    public function capturePayment($id)
    {
        return $this->makeRequest(
            'GET',
            '/v1/payments/'.$id
        );
    }
}
