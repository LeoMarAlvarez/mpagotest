<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'mercadopago'  =>  [
        'base_uri'      => 'https://api.mercadopago.com',
        'key'           => 'APP_USR-7eb0138a-189f-4bec-87d1-c0504ead5626',
        'secret'        => 'APP_USR-6317427424180639-042414-47e969706991d3a442922b0702a0da44-469485398',
        'base_currency' => 'ARS',
        'integrator_id' => 'dev_24c65fb163bf11ea96500242ac130004',
        'collector_id'  => '469485398'
    ]

];
