<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::POST('/detail','HomeController@getItem');

Route::get('/success', [
    'uses' =>'HomeController@success',
    'as' => 'success'
]);

Route::get('/pending', [
    'uses' =>'HomeController@pending',
    'as' => 'pending'
]);

Route::get('/failure', [
    'uses' =>'HomeController@failure',
    'as' => 'failure'
]);

Route::post('/notifications-ipn','HomeController@ipn')->name('ipn');
